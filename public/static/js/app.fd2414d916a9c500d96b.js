webpackJsonp([0],{

/***/ "18ZM":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/img/sprite.236a2c3.png";

/***/ }),

/***/ "2vtq":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "NHnr":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm.js
var vue_esm = __webpack_require__("7+uW");

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/Axis.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// общая высота
var Y = 80;
// пикселей в сантиметре
var STEP = 39;
// положение x для нуля
var MX = 35;
// толщина линии
var WIDTH = 2;
// максимальная длина дуги
var MAX_LENGTH = 350;
// параметры поля ввода a и b
var INPUT_WIDTH = 14;
var INPUT_MARGIN = 5;

function getPath(start, end, state) {
  var length = end - start;
  var QX = start + length / 2;
  state.coor.x = QX;
  var QY = -(2 * Y - WIDTH) / (MAX_LENGTH / length) + Y;
  state.coor.y = -(Y - WIDTH) / (MAX_LENGTH / length);
  return 'M ' + start + ', ' + Y + ' Q ' + QX + ', ' + QY + ', ' + end + ', ' + Y;
}

/* harmony default export */ var Axis = ({
  name: 'Axis',
  data: function data() {
    return {
      aState: {
        value: null,
        isShow: true,
        isInput: true,
        coor: {
          x: null,
          y: null
        },
        isError: false
      },
      bState: {
        value: null,
        isShow: false,
        isInput: true,
        coor: {
          x: null,
          y: null
        },
        isError: false
      },
      abState: {
        value: null,
        isInput: false,
        isError: false
      }
    };
  },

  props: {
    a: {
      type: Number,
      required: true
    },
    b: {
      type: Number,
      required: true
    }
  },
  computed: {
    abText: function abText() {
      return !this.aState.isInput && !this.bState.isInput ? this.a + this.b : '?';
    },
    // координата x точки a
    AX: function AX() {
      return this.a * STEP + MX;
    },
    // координата x точки b
    BX: function BX() {
      return this.b * STEP + this.AX;
    },
    // кривая Безье для a
    pathA: function pathA() {
      return {
        d: getPath(MX, this.AX, this.aState),
        style: 'marker-end: url(#MarkerArrow); stroke:#B6195E; stroke-width:' + WIDTH + '; fill:none;'
      };
    },
    // кривая Безье для b
    pathB: function pathB() {
      return {
        d: getPath(this.AX, this.BX, this.bState),
        style: 'marker-end: url(#MarkerArrow); stroke:#B6195E; stroke-width:' + WIDTH + '; fill:none;'
      };
    },
    // положение поля ввода a
    aStyleObject: function aStyleObject() {
      return {
        top: this.aState.coor.y + Y - INPUT_MARGIN + 'px',
        left: this.aState.coor.x - INPUT_WIDTH / 2 + 'px'
      };
    },
    // положение поля ввода b
    bStyleObject: function bStyleObject() {
      return {
        top: this.bState.coor.y + Y - INPUT_MARGIN + 'px',
        left: this.bState.coor.x - INPUT_WIDTH / 2 + 'px'
      };
    }
  },
  mounted: function mounted() {
    var _this = this;

    this.$nextTick(function () {
      _this.$refs.a.focus();
    });
  },
  methods: {
    checkA: function checkA() {
      var _this2 = this;

      if (Number(this.aState.value) === this.a) {
        this.aState.isInput = false;
        this.bState.isShow = true;
        this.aState.isError = false;
        this.$nextTick(function () {
          // теперь DOM обновлён
          _this2.$refs.b.focus();
        });
      } else {
        this.aState.isError = true;
      }
    },
    checkB: function checkB() {
      var _this3 = this;

      if (Number(this.bState.value) === this.b) {
        this.bState.isInput = false;
        this.abState.isInput = true;
        this.bState.isError = false;
        this.$nextTick(function () {
          // теперь DOM обновлён
          _this3.$refs.ab.focus();
        });
      } else {
        this.bState.isError = true;
      }
    },
    checkAB: function checkAB(event) {
      if (Number(this.abState.value) === this.a + this.b) {
        this.abState.isInput = false;
      } else {
        this.abState.isError = true;
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-1cc158a0","hasScoped":true,"transformToRequire":{"video":["src","poster"],"source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/Axis.vue
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"axios"},[_c('div',{staticClass:"axios__math"},[_c('span',{class:{ error_math: _vm.aState.isError }},[_vm._v(" "+_vm._s(_vm.a))]),_vm._v(" "),_c('span',[_vm._v("+")]),_vm._v(" "),_c('span',{class:{ error_math: _vm.bState.isError }},[_vm._v(" "+_vm._s(_vm.b))]),_vm._v(" "),_c('span',[_vm._v("=")]),_vm._v(" "),(_vm.abState.isInput)?_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.abState.value),expression:"abState.value"}],ref:"ab",class:{ error_input: _vm.abState.isError },attrs:{"tabindex":"3"},domProps:{"value":(_vm.abState.value)},on:{"input":[function($event){if($event.target.composing){ return; }_vm.$set(_vm.abState, "value", $event.target.value)},_vm.checkAB]}}):_c('span',[_vm._v(_vm._s(_vm.abText))])]),_vm._v(" "),_c('div',{staticClass:"axios__draw canvas"},[(_vm.aState.isShow)?_c('div',{staticClass:"canvas__num",style:(_vm.aStyleObject)},[(_vm.aState.isInput)?_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.aState.value),expression:"aState.value"}],ref:"a",class:{ error_input: _vm.aState.isError },attrs:{"tabindex":"1"},domProps:{"value":(_vm.aState.value)},on:{"input":[function($event){if($event.target.composing){ return; }_vm.$set(_vm.aState, "value", $event.target.value)},_vm.checkA]}}):_c('div',[_vm._v(_vm._s(_vm.a))])]):_vm._e(),_vm._v(" "),(_vm.bState.isShow)?_c('div',{staticClass:"canvas__num",style:(_vm.bStyleObject)},[(_vm.bState.isInput)?_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.bState.value),expression:"bState.value"}],ref:"b",class:{ error_input: _vm.bState.isError },attrs:{"tabindex":"2"},domProps:{"value":(_vm.bState.value)},on:{"input":[function($event){if($event.target.composing){ return; }_vm.$set(_vm.bState, "value", $event.target.value)},_vm.checkB]}}):_c('div',[_vm._v(_vm._s(_vm.b))])]):_vm._e(),_vm._v(" "),_c('svg',{staticClass:"canvas__svg"},[_c('defs',[_c('marker',{attrs:{"id":"MarkerArrow","viewBox":"0 0 10 10","refX":"10","refY":"5","markerUnits":"userSpaceOnUse","orient":"auto","markerWidth":"10","markerHeight":"10"}},[_c('polyline',{attrs:{"id":"markerPoly1","points":"0,0 10,5 0,10 2,5","fill":"#B6195E"}})])]),_vm._v(" "),(_vm.aState.isShow)?_c('path',_vm._b({},'path',_vm.pathA,false)):_vm._e(),_vm._v(" "),(_vm.bState.isShow)?_c('path',_vm._b({},'path',_vm.pathB,false)):_vm._e()]),_vm._v(" "),_c('img',{staticClass:"image",attrs:{"src":__webpack_require__("18ZM")}})])])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ var components_Axis = (esExports);
// CONCATENATED MODULE: ./src/components/Axis.vue
function injectStyle (ssrContext) {
  __webpack_require__("2vtq")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1cc158a0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  Axis,
  components_Axis,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ var src_components_Axis = (Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/App.vue
//
//
//
//
//
//



var MIN_A = 6;
var MAX_A = 9;
var MIN_A_B = 11;
var MAX_A_B = 14;

function getInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/* harmony default export */ var App = ({
  name: 'app',
  components: {
    Axis: src_components_Axis
  },
  computed: {
    numbers: function numbers() {
      var a = getInt(MIN_A, MAX_A);
      var b = getInt(MIN_A_B - a, MAX_A_B - a);
      return { a: a, b: b };
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-62046e7d","hasScoped":false,"transformToRequire":{"video":["src","poster"],"source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/App.vue
var App_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{attrs:{"id":"app"}},[_c('Axis',_vm._b({},'Axis',_vm.numbers,false))],1)}
var App_staticRenderFns = []
var App_esExports = { render: App_render, staticRenderFns: App_staticRenderFns }
/* harmony default export */ var selectortype_template_index_0_src_App = (App_esExports);
// CONCATENATED MODULE: ./src/App.vue
function App_injectStyle (ssrContext) {
  __webpack_require__("hIdk")
}
var App_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var App___vue_template_functional__ = false
/* styles */
var App___vue_styles__ = App_injectStyle
/* scopeId */
var App___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var App___vue_module_identifier__ = null
var App_Component = App_normalizeComponent(
  App,
  selectortype_template_index_0_src_App,
  App___vue_template_functional__,
  App___vue_styles__,
  App___vue_scopeId__,
  App___vue_module_identifier__
)

/* harmony default export */ var src_App = (App_Component.exports);

// CONCATENATED MODULE: ./src/main.js
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.



vue_esm["a" /* default */].config.productionTip = false;

/* eslint-disable no-new */
new vue_esm["a" /* default */]({
  el: '#app',
  template: '<App/>',
  components: { App: src_App }
});

/***/ }),

/***/ "hIdk":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

},["NHnr"]);
//# sourceMappingURL=app.fd2414d916a9c500d96b.js.map