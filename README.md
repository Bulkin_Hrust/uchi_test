# Ось-2

Тестовое задание для [вакансии](https://hh.ru/vacancy/23881884) uchi.ru

Выполнено как отдельный переиспользуемый компонент src/components/Axis.vue

Для графики использован нативный svg

Размещено на gitlab pages --> [демо](https://bulkin_hrust.gitlab.io/uchi_test/)

Добавлены основные мета теги

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build
```

## Built With

[vue.js](https://ru.vuejs.org/v2/guide/index.html) - прогрессивный фрейворк для создания пользовательских интерфейсов

[stylus](http://stylus-lang.com/) - современный и очень удобный препроцессор
